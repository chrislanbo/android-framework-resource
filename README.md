#android-framework-resource
用于工作中android的源码查看注释以及修改

#####2016年6月2日11:55:46
	int android.view.View.getTop()
	返回当前view顶部相对于父view顶部的px值
	int android.view.View.getBottom()
	返回当前view底部相对于父view底部的px值
	int android.view.View.getLeft()
	返回当前view左边缘相对于父view左边缘的px值
	int android.view.View.getRight()
	返回当前view右边缘相对于父view右边缘的px值

	int android.view.View.getHeight()
	返回当前view的height的px值，源码中为mBottom - mTop
	
	int android.view.View.getWidth()
	返回当前view的height的px值，源码中为mRight - mLeft




View android.view.View.findFocus()
当前View为根的view层次结构中，获得当前输入焦点的view，return 当前焦点的view ，没有焦点view return null
View android.view.ViewGroup.findFocus()
DBug会打印当前view类对象、当前view是否有焦点、子view
如果当前view有焦点，直接返回当前类，否则子view不为null，子view调用view类的findFocus()返回焦点或者null

